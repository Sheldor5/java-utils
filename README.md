# JavaUtils

Utilities for common Java capabilities.

### ZipUtils - ZIP Compression/Extraction

```java
byte[] data = ...;

byte[] compressed = ZipUtils.compress(data);
byte[] extracted = ZipUtils.extract(compressed);
```

### SecurityUtils - Key Material Loading (DER, PKCS12, JKS)

Also look at [gen.sh](src/test/resources/gen.sh) for common `openssl`/`keytool` commands to generate/convert `PEM`/`DER`-encoded public/private keys and `JKS`/`PKCS12` keystores!

##### Load DER-encoded private Key

```java
PrivateKey privateKey = SecurityUtils.loadDerEncodedKey("key.der");
```
	
##### Load DER-encoded X.509 Certificate

```java
Certificate certificate = SecurityUtils.loadDerEncodedCertificate("cert.der");
```

### Code Snipets which worked with Java < 9

##### Generate X.509 Certificate from PublicKey

```java
import sun.security.x509.*;

public class SecurityUtils {

	private static final String CERT_SIGNING_ALGORITHM = "SHA256withRSA";

	public static X509Certificate generateSelfSignedCertificate(final String dn, final KeyPair keyPair, final int days) throws Exception {
		final PrivateKey privateKey = keyPair.getPrivate();
		final X509CertInfo x509CertInfo = new X509CertInfo();
		final Date from = new Date();
		final Date to = new Date(from.getTime() + days * 86_400_000L);
		final CertificateValidity interval = new CertificateValidity(from, to);
		final BigInteger sn = new BigInteger(64, new SecureRandom());
		final X500Name owner = new X500Name(dn);
		final AlgorithmId algorithmId = new AlgorithmId(AlgorithmId.sha256WithRSAEncryption_oid);

		x509CertInfo.set(X509CertInfo.VALIDITY, interval);
		x509CertInfo.set(X509CertInfo.SERIAL_NUMBER, new CertificateSerialNumber(sn));
		//x509CertInfo.set(X509CertInfo.SUBJECT, new CertificateSubjectName(owner));
		x509CertInfo.set(X509CertInfo.SUBJECT, owner);
		//x509CertInfo.set(X509CertInfo.ISSUER, new CertificateIssuerName(owner));
		x509CertInfo.set(X509CertInfo.ISSUER, owner);
		x509CertInfo.set(X509CertInfo.KEY, new CertificateX509Key(keyPair.getPublic()));
		x509CertInfo.set(X509CertInfo.VERSION, new CertificateVersion(CertificateVersion.V3));
		x509CertInfo.set(X509CertInfo.ALGORITHM_ID, new CertificateAlgorithmId(algorithmId));

		// Sign the cert to identify the algorithm that's used.
		final X509CertImpl x509Cert = new X509CertImpl(x509CertInfo);
		x509Cert.sign(privateKey, CERT_SIGNING_ALGORITHM);

		// Update the algorithm, and resign.
		final AlgorithmId updatedAlgorithmId = (AlgorithmId) x509Cert.get(X509CertImpl.SIG_ALG);
		x509CertInfo.set(CertificateAlgorithmId.NAME + "." + CertificateAlgorithmId.ALGORITHM, updatedAlgorithmId);
		final X509CertImpl x509Certificate = new X509CertImpl(x509CertInfo);
		x509Certificate.sign(privateKey, CERT_SIGNING_ALGORITHM);
		return x509Certificate;
	}
}
```

### ???

```java
kkk
```

