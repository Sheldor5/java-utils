package at.palata.utils.base64;

import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.Objects;

public class Base64InputStream extends InputStream {

    private final Base64.Decoder decoder = Base64.getDecoder();
    private final String source;

    private byte[] buffer = new byte[3];

    private int sourceIndex = 0;
    private int bufferIndex = 3;

    public Base64InputStream(final String source) {
        Objects.requireNonNull(source);
        if (source.length() % 4 != 0) {
            throw new IllegalArgumentException("Source length must be a multiple of 4 to be a valid Base64 String");
        }
        this.source = source;
    }

    @Override
    public int read() {
        if (this.bufferIndex == this.buffer.length) {
            if (this.source.length() == this.sourceIndex) {
                return -1;
            }
            this.buffer = decoder.decode(this.source.substring(this.sourceIndex, this.sourceIndex + 4));
            this.sourceIndex += 4;
            this.bufferIndex = 0;
        }
        return Byte.toUnsignedInt(this.buffer[this.bufferIndex++]);
    }

    @Override
    public int read(byte[] b) throws IOException {
        return super.read(b);
    }
}
