package at.palata.utils.math;

public record Coordinate(double x, double y) {
}