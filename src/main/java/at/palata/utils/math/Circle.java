package at.palata.utils.math;

import java.util.List;

public record Circle(double radius, List<Coordinate> dots) {
}