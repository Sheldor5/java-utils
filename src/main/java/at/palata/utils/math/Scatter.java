package at.palata.utils.math;

import java.util.ArrayList;
import java.util.List;

public class Scatter {

    public static Circle scatterDotsInCircle(final int dots, final double radius) {
        final List<Coordinate> coordinates = new ArrayList<>(dots);
        double p = (2 * Math.PI) / dots;
        double a = 0.0D, x, y;
        for (int i = 0; i < dots; i++) {
            x = radius * Math.cos(a);
            y = radius * Math.sin(a);
            coordinates.add(new Coordinate(x, y));
            a += p;
        }
        return new Circle(radius, coordinates);
    }
}