package at.palata.utils.security;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.security.*;
import java.security.cert.*;
import java.security.cert.Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

@Slf4j
public class SecurityUtils {

	public static KeyPair generateKeyPair() {
		return generateKeyPair(4096);
	}

	public static KeyPair generateKeyPair(final int keySize) {
		log.trace("Generating new KeyPair with keySize={} ...", keySize);
		try {
			final KeyPairGenerator keyPairGenerator = KeyPairGenerator.getInstance("RSA");
			keyPairGenerator.initialize(keySize);
			final KeyPair keyPair = keyPairGenerator.generateKeyPair();
			log.trace("Generated new KeyPair with keySize={}", keySize);
			return keyPair;
		} catch (final NoSuchAlgorithmException e) {
			log.error("Error while generating KeyPair: ", e);
			return null; // should never happen ...
		}
	}

	public static KeyStore loadKeyStore(final String path, final String password) throws KeyStoreException, IOException, CertificateException, NoSuchAlgorithmException {
		log.trace("Loading KeyStore from {} ...", path);
		final KeyStore store = KeyStore.getInstance(KeyStore.getDefaultType());
		try (final FileInputStream inputStream = new FileInputStream(path)) {
			store.load(inputStream, password.toCharArray());
			log.trace("Loaded KeyStore from {}", path);
			return store;
		}
	}

	public static void saveCertificate(final Certificate certificate, final File keyFile) throws CertificateEncodingException, IOException {
		log.trace("Saving Certificate to {} ...", keyFile.getAbsolutePath());
		final X509EncodedKeySpec x509EncodedKeySpec = new X509EncodedKeySpec(certificate.getEncoded());
		try (final FileOutputStream fos = new FileOutputStream(keyFile.getAbsoluteFile())) {
			fos.write(x509EncodedKeySpec.getEncoded());
			log.trace("Saved Certificate to {}", keyFile.getAbsolutePath());
		}
	}

	public static Certificate loadDerEncodedCertificate(final String path) throws FileNotFoundException, CertificateException {
		log.trace("Loading Certificate from {} ...", path);
		final CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
		try (final FileInputStream fis = new FileInputStream(path)) {
			final Certificate certificate = certificateFactory.generateCertificate(fis);
			log.trace("Loaded Certificate from {}", path);
			return certificate;
		} catch (IOException e) {
			throw new CertificateException();
		}
	}

	public static void savePrivateKey(final PrivateKey privateKey, final File keyFile) throws IOException {
		final PKCS8EncodedKeySpec pkcs8EncodedKeySpec = new PKCS8EncodedKeySpec(privateKey.getEncoded());
		try (final FileOutputStream fos = new FileOutputStream(keyFile.getAbsoluteFile())) {
			fos.write(pkcs8EncodedKeySpec.getEncoded());
		}
	}

	public static PrivateKey loadDerEncodedKey(final String path) throws IOException, InvalidKeySpecException {
		log.trace("Loading Key from {} ...", path);
		try {
			final KeyFactory keyFactory = KeyFactory.getInstance("RSA");
			final File file = new File(path);
			try (final DataInputStream dataInputStream = new DataInputStream(new FileInputStream(file))) {
				final byte[] keyBytes = new byte[(int) file.length()];
				dataInputStream.readFully(keyBytes);
				final PrivateKey privateKey = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(keyBytes));
				log.trace("Loaded Key from {}", path);
				return privateKey;
			}
		} catch (final NoSuchAlgorithmException e) {
			log.error("Error while loading Key: ", e);
			return null; // should never happen ...
		}
	}
}
