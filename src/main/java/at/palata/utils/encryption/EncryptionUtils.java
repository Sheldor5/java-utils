package at.palata.utils.encryption;

import java.io.File;
import java.io.IOException;

public final class EncryptionUtils {

    public static final class OneTimePad {
        private static final at.palata.utils.encryption.OneTimePad ONE_TIME_PAD = new at.palata.utils.encryption.OneTimePad();

        public static void encrypt(final File fileToEncrypt, final File padFileDestination, final File encryptedFileDestination) throws IOException {
            ONE_TIME_PAD.encrypt(fileToEncrypt, padFileDestination, encryptedFileDestination);
        }

        public static void decrypt(final File fileToDecrypt, final File padFileSource, final File decryptedFileDestination) throws IOException {
            ONE_TIME_PAD.decrypt(fileToDecrypt, padFileSource, decryptedFileDestination);
        }
    }
}
