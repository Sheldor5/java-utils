package at.palata.utils.encryption;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.Random;

public final class OneTimePad {

    private static final Random DEFAULT_RANDOM = new SecureRandom();
    private static final int DEFAULT_BUFFER_SIZE = 1024;

    private final Random random;
    private final int bufferSize;

    public OneTimePad() {
        this(DEFAULT_RANDOM, DEFAULT_BUFFER_SIZE);
    }

    public OneTimePad(final Random random, final int bufferSize) {
        this.random = random;
        this.bufferSize = bufferSize;
    }

    public void encrypt(final File fileToEncrypt, final File padFileDestination, final File encryptedFileDestination) throws IOException {
        try (final FileInputStream dfis = new FileInputStream(fileToEncrypt);
             final FileOutputStream pfos = new FileOutputStream(padFileDestination);
             final FileOutputStream efos = new FileOutputStream(encryptedFileDestination)) {

            final byte[] dfisBuffer = new byte[bufferSize];
            final byte[] efosBuffer = new byte[bufferSize];
            final byte[] pfosBuffer = new byte[bufferSize];

            int d;
            while ((d = dfis.read(dfisBuffer)) != -1) {
                this.random.nextBytes(pfosBuffer);
                for (int i = 0; i < d; i++) {
                    efosBuffer[i] = (byte) (dfisBuffer[i] ^ pfosBuffer[i]);
                }
                efos.write(efosBuffer, 0, d);
                pfos.write(pfosBuffer, 0, d);
            }
        }
    }

    public void decrypt(final File fileToDecrypt, final File padFileSource, final File decryptedFileDestination) throws IOException {
        try (final FileInputStream efis = new FileInputStream(fileToDecrypt);
             final FileInputStream pfis = new FileInputStream(padFileSource);
             final FileOutputStream dfos = new FileOutputStream(decryptedFileDestination)) {

            final byte[] efisBuffer = new byte[bufferSize];
            final byte[] pfisBuffer = new byte[bufferSize];
            final byte[] dfosBuffer = new byte[bufferSize];

            int e, p;
            while ((e = efis.read(efisBuffer)) != -1 && (p = pfis.read(pfisBuffer)) != -1) {
                if (e != p) {
                    throw new IOException(""); // TODO
                }
                for (int i = 0; i < e; i++) {
                    dfosBuffer[i] = (byte) (efisBuffer[i] ^ pfisBuffer[i]);
                }
                dfos.write(dfosBuffer, 0, e);
            }
        }
    }
}
