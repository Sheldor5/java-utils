package at.palata.utils.zip;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.zip.*;

public final class ZipUtils {

	private ZipUtils() {
		// defeat instantiation
	}

	/**
	 * See {@link ZipUtils#compress(byte[])}.
	 */
	public static byte[] deflate(final byte[] bytes) throws IOException {
		return compress(bytes);
	}

	/**
	 * ZIP-compress a byte array.
	 *
	 * @param bytes input bytes to compress.
	 * @return compressed output bytes.
	 * @throws IOException if an IO error occurs.
	 */
	public static byte[] compress(final byte[] bytes) throws IOException {
		return ZipCompressionUtils.compress(bytes);
	}

	/**
	 * See {@link ZipUtils#compress(InputStream, OutputStream)}.
	 */
	public static void deflate(final InputStream inputStream, final OutputStream outputStream) throws IOException {
		compress(inputStream, outputStream);
	}

	/**
	 * ZIP-compress the bytes from an {@link InputStream} to an {@link OutputStream}.
	 *
	 * @param inputStream the raw bytes to read.
	 * @param outputStream the compressed bytes to write.
	 * @return
	 * @throws IOException
	 */
	public static void compress(final InputStream inputStream, final OutputStream outputStream) throws IOException {
		ZipCompressionUtils.compress(inputStream, outputStream);
	}

	/**
	 *
	 * See {@link ZipUtils#extract(byte[])}.
	 */
	public static byte[] inflate(final byte[] bytes) throws DataFormatException, IOException {
		return extract(bytes);
	}

	/**
	 *
	 * @param bytes
	 * @return
	 * @throws DataFormatException
	 * @throws IOException
	 */
	public static byte[] extract(final byte[] bytes) throws DataFormatException, IOException {
		return ZipExtractionUtils.extract(bytes);
	}

	/**
	 * See {@link ZipUtils#extract(InputStream, OutputStream)}.
	 */
	public static void inflate(final InputStream inputStream, final OutputStream outputStream) throws DataFormatException, IOException {
		extract(inputStream, outputStream);
	}

	/**
	 *
	 * @param inputStream
	 * @param outputStream
	 * @throws IOException
	 */
	public static void extract(final InputStream inputStream, final OutputStream outputStream) throws DataFormatException, IOException {
		ZipExtractionUtils.extract(inputStream, outputStream);
	}
}
