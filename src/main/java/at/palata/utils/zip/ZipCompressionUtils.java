package at.palata.utils.zip;

import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.zip.Deflater;

@Slf4j
final class ZipCompressionUtils {

	private static final DecimalFormat COMPRESSION_RATE_FORMAT = new DecimalFormat("##");
	private static final int BUFFER_SIZE = 512;

	static {
		COMPRESSION_RATE_FORMAT.setRoundingMode(RoundingMode.DOWN);
	}

	private ZipCompressionUtils() {
		// defeat instantiation
	}

	public static byte[] compress(final byte[] bytes) throws IOException {
		log.trace("Compressing {} bytes ...", bytes.length);

		final Deflater deflater = new Deflater(Deflater.DEFAULT_COMPRESSION, true);
		deflater.setInput(bytes);
		deflater.finish();

		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(bytes.length);
		final byte[] buffer = new byte[BUFFER_SIZE];

		int count;
		while (!deflater.finished()) {
			count = deflater.deflate(buffer);
			byteArrayOutputStream.write(buffer, 0, count);
		}

		byteArrayOutputStream.flush();
		byteArrayOutputStream.close();

		final byte[] compressed = byteArrayOutputStream.toByteArray();

		log.trace("Compressed {} bytes to {} bytes ({}%)", bytes.length, compressed.length, formatCompressionRate(bytes.length, compressed.length));
		return compressed;
	}

	static void compress(final InputStream inputStream, final OutputStream outputStream) throws IOException {
		log.trace("Compressing {} to {} ...", inputStream, outputStream);

		final Deflater deflater = new Deflater(Deflater.DEFAULT_COMPRESSION, true);

		final byte[] inputStreamBuffer = new byte[BUFFER_SIZE];
		final byte[] outputStreamBuffer = new byte[BUFFER_SIZE];

		long original = 0, compressed = 0;
		int count = 0;

		while (!deflater.finished()) {
			while (!deflater.finished() && deflater.needsInput() && count != -1) {
				count = inputStream.read(inputStreamBuffer);
				if (count <= 0) {
					deflater.finish();
				} else {
					original += count;
					deflater.setInput(inputStreamBuffer, 0, count);
				}
			}

			do {
				count = deflater.deflate(outputStreamBuffer);
				if (count > 0) {
					outputStream.write(outputStreamBuffer, 0, count);
					compressed += count;
				}
			} while (count > 0);
		}

		inputStream.close();
		outputStream.flush();
		outputStream.close();

		log.trace("Compressed {} bytes to {} bytes ({}%)", original, compressed, formatCompressionRate(original, compressed));
	}

	private static String formatCompressionRate(double input, double output) {
		return COMPRESSION_RATE_FORMAT.format((output / input) * 100.0d);
	}
}
