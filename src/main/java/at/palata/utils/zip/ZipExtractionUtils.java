package at.palata.utils.zip;

import lombok.extern.slf4j.Slf4j;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

@Slf4j
final class ZipExtractionUtils {

	private static final DecimalFormat EXTRACTION_RATE_FORMAT = new DecimalFormat("##");
	private static final int BUFFER_SIZE = 512;

	private ZipExtractionUtils() {
		// defeat instantiation
	}

	static {
		EXTRACTION_RATE_FORMAT.setRoundingMode(RoundingMode.UP);
	}

	static byte[] extract(final byte[] bytes) throws DataFormatException, IOException {
		log.trace("Extracting from {} bytes ...", bytes.length);

		final Inflater inflater = new Inflater(true);
		inflater.setInput(bytes);

		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(5 * bytes.length);
		final byte[] buffer = new byte[BUFFER_SIZE];

		int count;
		while (!inflater.finished()) {
			count = inflater.inflate(buffer);
			byteArrayOutputStream.write(buffer, 0, count);
		}

		byteArrayOutputStream.flush();
		byteArrayOutputStream.close();

		final byte[] extracted = byteArrayOutputStream.toByteArray();

		log.trace("Extracted {} bytes from {} bytes ({}%)", extracted.length, bytes.length, formatExtractionRate(bytes.length, extracted.length));
		return extracted;
	}

	static void extract(final InputStream inputStream, final OutputStream outputStream) throws DataFormatException, IOException {
		log.trace("Extracting from {} ...", inputStream);

		final Inflater inflater = new Inflater(true);

		final byte[] inputStreamBuffer = new byte[BUFFER_SIZE];
		final byte[] outputStreamBuffer = new byte[BUFFER_SIZE];

		long compressed = 0, original = 0;
		int count = 0;

		while (!inflater.finished()) {
			while (!inflater.finished() && inflater.needsInput() && count != -1) {
				count = inputStream.read(inputStreamBuffer);
				if (count > 0) {
					compressed += count;
					inflater.setInput(inputStreamBuffer, 0, count);
				}
			}

			do {
				count = inflater.inflate(outputStreamBuffer);
				if (count > 0) {
					outputStream.write(outputStreamBuffer, 0, count);
					original += count;
				}
			} while (count > 0);
		}

		inputStream.close();
		outputStream.flush();
		outputStream.close();

		log.trace("Extracted {} bytes from {} bytes ({}%)", original, compressed, formatExtractionRate(compressed, original));
	}

	private static String formatExtractionRate(double input, double output) {
		return EXTRACTION_RATE_FORMAT.format((output / input) * 100.0d);
	}
}
