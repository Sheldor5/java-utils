#!/usr/bin/env bash

readonly OUTPUT_DIRECTORY=./credentials
readonly LIFETIME=3652
readonly KEYSIZE=4096
readonly PASWORD=changeit

if [[ "${OSTYPE}" == "cygwin" || "${OSTYPE}" == "msys" || "${OSTYPE}" == "win32" ]]; then
  readonly SUBJECT_NAME='//CN=localhost'
else
  readonly SUBJECT_NAME='/CN=localhost'
fi

mkdir -p "${OUTPUT_DIRECTORY}"

echo "Generating Key-Certificate-Pair ..."
openssl req -new -x509 -nodes -newkey "rsa:${KEYSIZE}" -keyout "${OUTPUT_DIRECTORY}/key.pem" -days "${LIFETIME}" -subj "${SUBJECT_NAME}" -out "${OUTPUT_DIRECTORY}/cert.pem"

echo "Converting PEM-encoded PKCS#1 Key into DER-encoded PKCS#8 Key ..."
openssl pkcs8 -topk8 -nocrypt -inform PEM -outform DER -in "${OUTPUT_DIRECTORY}/key.pem" -out "${OUTPUT_DIRECTORY}/key.der"

echo "Converting PEM-encoded Certificate into DER-encoded Certificate ..."
openssl x509 -outform DER -in "${OUTPUT_DIRECTORY}/cert.pem" -out "${OUTPUT_DIRECTORY}/cert.der"

echo "Creating PKCS#12 KeyStore ..."
cat "${OUTPUT_DIRECTORY}/key.pem" > "${OUTPUT_DIRECTORY}/server.pem"
cat "${OUTPUT_DIRECTORY}/cert.pem" >> "${OUTPUT_DIRECTORY}/server.pem"
openssl pkcs12 -export -nodes -in "${OUTPUT_DIRECTORY}/server.pem" -out "${OUTPUT_DIRECTORY}/server.pkcs12" -passout "pass:${PASWORD}"

echo "Converting PKCS#12 KeyStore into (deprecated) JKS KeyStore ..."
keytool -importkeystore -srckeystore "${OUTPUT_DIRECTORY}/server.pkcs12" -srcstoretype pkcs12 -srcstorepass "${PASWORD}" -destkeystore "${OUTPUT_DIRECTORY}/server.jks" -deststoretype JKS -deststorepass "${PASWORD}"
