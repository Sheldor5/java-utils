package at.palata.utils.encryption;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import static org.junit.jupiter.api.Assertions.*;

class EncryptionUtilsTest {

    @Test
    void testOneTimePad() throws IOException {
        final File encryptedFile = Files.createTempFile("", ".ef").toFile();
        final File padFile = Files.createTempFile("", ".pf").toFile();
        final File decryptedFile = Files.createTempFile("", ".txt").toFile();

        try {
            final File fileToDecrypt = new File("src/test/resources/top_secret.txt");

            EncryptionUtils.OneTimePad.encrypt(fileToDecrypt, padFile, encryptedFile);

            final byte[] a = Files.readAllBytes(fileToDecrypt.toPath());
            final byte[] b = Files.readAllBytes(encryptedFile.toPath());
            assertEquals(a.length, b.length);
            int e = 0;
            for (int i = 0; i < a.length; i++) {
                if (a[i] == b[i]) {
                    e++;
                }
            }
            assertNotEquals(a.length, e);

            EncryptionUtils.OneTimePad.decrypt(encryptedFile, padFile, decryptedFile);

            final byte[] d = Files.readAllBytes(decryptedFile.toPath());
            assertArrayEquals(a, d);
        } finally {
            encryptedFile.delete();
            padFile.delete();
            decryptedFile.delete();
        }
    }
}