package at.palata.utils.zip;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

class ZipUtilsTest {

	private static final String testFile = "src/test/resources/gen.sh";

	@Test
	void all() throws Exception {
		final byte[] data = randomString().getBytes(StandardCharsets.UTF_8);

		final byte[] compressed = ZipUtils.deflate(data);
		final byte[] extracted = ZipUtils.inflate(compressed);

		assertArrayEquals(data, extracted);
	}

	@Test
	void testCompressStreams(@TempDir final File testFolder) throws Exception {
		final File originalFile = new File(testFile);
		final FileInputStream fileInputStream1 = new FileInputStream(originalFile);

		final File compressedFile = File.createTempFile("compressed-", ".zip", testFolder);
		final FileOutputStream fileOutputStream1 = new FileOutputStream(compressedFile);

		final File extractedFile = File.createTempFile("decompressed-", "", testFolder);
		final FileInputStream fileInputStream2 = new FileInputStream(compressedFile);
		final FileOutputStream fileOutputStream2 = new FileOutputStream(extractedFile);
		try {
			ZipUtils.deflate(fileInputStream1, fileOutputStream1);
		} finally {
			fileInputStream1.close();
			fileOutputStream1.flush();
			fileOutputStream1.close();
		}
		try {
			ZipUtils.inflate(fileInputStream2, fileOutputStream2);
		} finally {
			fileInputStream2.close();
			fileOutputStream2.flush();
			fileOutputStream2.close();
		}

		assertEquals(originalFile.length(), extractedFile.length());
	}

	private static String randomString() {
		final StringBuilder stringBuilder = new StringBuilder(32_768);
		for (int i = 0; i < 100; i++) {
			stringBuilder.append(UUID.randomUUID().toString().replace("-", ""));
		}
		return stringBuilder.toString();
	}
}
