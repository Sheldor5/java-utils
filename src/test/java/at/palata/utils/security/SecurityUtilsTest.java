package at.palata.utils.security;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.security.*;
import java.security.cert.Certificate;

import static org.junit.jupiter.api.Assertions.*;

class SecurityUtilsTest {

	private static final String keyPath = "src/test/resources/credentials/key.der";
	private static final String certPath = "src/test/resources/credentials/cert.der";
	private static final String pkcs12KeyStorePath = "src/test/resources/credentials/server.pkcs12";
	private static final String jksKeyStorePath = "src/test/resources/credentials/server.jks";
	private static final String keyStorePassword = "changeit";

	@Test
	void all(@TempDir final File testFolder) throws Exception {
		// generateKeyPair()
		final KeyPair keyPair = SecurityUtils.generateKeyPair();
		assertNotNull(keyPair);

		final PrivateKey privateKey = keyPair.getPrivate();
		assertNotNull(privateKey);
		final File tmpKeyFile = File.createTempFile("key-", ".key", testFolder);

		// savePrivateKey()
		SecurityUtils.savePrivateKey(privateKey, tmpKeyFile);

		// loadDerEncodedKey()
		final PrivateKey key = SecurityUtils.loadDerEncodedKey(tmpKeyFile.getAbsolutePath());
		assertNotNull(key);

		assertEquals(privateKey, key);

		final PublicKey publicKey =	keyPair.getPublic();
		assertNotNull(publicKey);
		final File tmpCertFile = File.createTempFile("cert-", ".crt", testFolder);

		/* Java < 9 !!!
		// saveCertificate()
		final X509Certificate cert = SecurityUtils.generateSelfSignedCertificate("CN=localhost", keyPair, 3652);
		SecurityUtils.saveCertificate(cert, tmpCertFile);

		// loadDerEncodedCertificate()
		final Certificate certificate = SecurityUtils.loadDerEncodedCertificate(tmpCertFile.getAbsolutePath());
		assertNotNull(certificate);

		assertEquals("saved and loaded Certificate must match", cert, certificate);*/

		final Certificate cert = SecurityUtils.loadDerEncodedCertificate(certPath);
		SecurityUtils.saveCertificate(cert, tmpCertFile);

		final Certificate certificate = SecurityUtils.loadDerEncodedCertificate(tmpCertFile.getAbsolutePath());
		assertEquals(cert, certificate);
	}

	@Test
	void loadKeyStore() throws Exception {
		final KeyStore pkcs12keyStore = SecurityUtils.loadKeyStore(pkcs12KeyStorePath, keyStorePassword);
		assertNotNull(pkcs12keyStore);

		final Key key = pkcs12keyStore.getKey("1", keyStorePassword.toCharArray());
		assertNotNull(key);

		final Certificate certificate = pkcs12keyStore.getCertificate("1");
		assertNotNull(certificate);

		final KeyStore jksKeyStore = SecurityUtils.loadKeyStore(jksKeyStorePath, keyStorePassword);
		assertNotNull(jksKeyStore);
	}

	@Test
	void loadDerEncodedCertificate() throws Exception {
		final Certificate certificate = SecurityUtils.loadDerEncodedCertificate(certPath);
		assertNotNull(certificate);
	}

	@Test
	void loadDerEncodedKey() throws Exception {
		final PrivateKey privateKey = SecurityUtils.loadDerEncodedKey(keyPath);
		assertNotNull(privateKey);
	}
}
